﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squad : MonoBehaviour
{
    public SoldierData soldierData;

    public List<SquadUnit> members = new List<SquadUnit>();

    protected List<SquadUnit> enemyUnits = new List<SquadUnit>();

    [HideInInspector] public SplineSample squadRoadSample;

    public Road road;

    protected virtual void Start()
    {
        members.ForEach(m => m.Setup(this));
    }

    protected virtual void Update()
    {

    }

    protected virtual void AddMember(Soldier soldier)
    {
        members.Add(soldier);
    }

    protected virtual void RemoveMember(Soldier soldier)
    {
        members.Remove(soldier);
    }

    public void AddNeutral(NeutralSoldier neutral)
    {
        var go = neutral.gameObject;

        Destroy(neutral);

        if (!members.Exists(m => m.gameObject == go))
        {
            var soldier = go.AddComponent<PlayerSoldier>();

            soldier.Setup(this);

            AddMember(soldier);
        }
    }

    public virtual void MemberKilled(Soldier soldier)
    {
        RemoveMember(soldier);
    }

    public void AddEnemySquad(Squad enemySquad)
    {
        if (enemyUnits.Count > 0) return;

        foreach (var item in enemySquad.members)
        {
            enemyUnits.Add(item);
        }

        enemySquad.AddEnemySquad(this);

    }
}
