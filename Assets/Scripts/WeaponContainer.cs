﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class WeaponContainer : MonoBehaviour
{
    private Soldier soldier;

    [HideInInspector] public Weapon weapon;

    public void Setup(Soldier soldier)
    {
        this.soldier = soldier;

        enabled = true;

        weapon = transform.GetComponentInChildren<Weapon>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!soldier)
        {
            enabled = false;

            return;
        }

        //transform.forward = soldier.transform.forward; 
    }

}
