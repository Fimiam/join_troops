using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank : SquadUnit
{
    [SerializeField] private TankTower tower;

    [SerializeField] private float towerRotationSpeed = 12, idleMoveSpeed = 3;

    [SerializeField] private float allowFireAngle = 3, moveOffset = 2;

    [SerializeField] private List<TankWheel> wheels;

    private Coroutine idleRoutine;

    public override void Setup(Squad squad)
    {
        base.Setup(squad);

        idleRoutine = StartCoroutine(IdleMovement());
    }

    protected override void Update()
    {
        var hasTarget = target != null;

        if (!hasTarget)
        {
            enabled = false;

            return;
        }

        var targetDir = target.transform.position - transform.position;

        targetDir.y = 0;

        targetDir.Normalize();

        var targetRotation = Quaternion.LookRotation(targetDir);

        tower.transform.rotation = Quaternion.RotateTowards(tower.transform.rotation, targetRotation, towerRotationSpeed * Time.deltaTime);

        var currentDir = tower.transform.forward;

        currentDir.y = 0;

        var angleBetween = Vector3.Angle(currentDir, targetDir);

        //Debug.Log(angleBetween);
        //Debug.Log(tower.CanFire);

        if(angleBetween < allowFireAngle/* && tower.CanFire*/)
        {
            tower.Fire(target);
        }

    }

    public override void SetTarget(SquadUnit target)
    {
        base.SetTarget(target);

        enabled = true;

        if(target != null)
        {
            StopCoroutine(idleRoutine);
            idleRoutine = null;
        }
        else
        {
            if(idleRoutine == null)
            idleRoutine = StartCoroutine(IdleMovement());
        }
    }

    private IEnumerator IdleMovement()
    {
        var targetOffset = moveOffset;

        var pos = transform.localPosition;

        float posDelta = default, prev = default;

        while(true)
        {

            prev = pos.x;

            pos.x = Mathf.MoveTowards(pos.x, targetOffset, idleMoveSpeed * Time.deltaTime);

            //pos.x = Mathf.SmoothStep(-2, 2, pos.x);

            transform.localPosition = Vector3.Lerp(transform.localPosition, pos, 4* Time.deltaTime);


            posDelta = pos.x - prev;

            foreach (var item in wheels)
            {
                item.UpdateMoveDelta(posDelta);
            }

            if((targetOffset < 0 && pos.x <= targetOffset) || (targetOffset > 0 && pos.x >= targetOffset))
            {
                targetOffset *= -1.0f;

                var towerInitTurn = tower.transform.localRotation.eulerAngles.y;

                float towerNextTurn = towerInitTurn < 180 ? 180.5f : 0;

                while(towerInitTurn != towerNextTurn)
                {
                    towerInitTurn = Mathf.MoveTowardsAngle(towerInitTurn, towerNextTurn, 168 * Time.deltaTime);

                    tower.transform.localRotation = Quaternion.Euler(Vector3.up * towerInitTurn);

                    yield return null;
                }    
            }
            

            yield return null;
        }

    }
}
