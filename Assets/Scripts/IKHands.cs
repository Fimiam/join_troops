﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKHands : MonoBehaviour
{
    private Transform rHandPoint, lHandPoint;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void SetupHands(Transform rHand, Transform lHand)
    {
        rHandPoint = rHand;
        lHandPoint = lHand;
    }

    private void OnAnimatorIK()
    {
        if(rHandPoint)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            animator.SetIKPosition(AvatarIKGoal.RightHand, rHandPoint.position);
        }
        else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
        }

        if (lHandPoint)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            animator.SetIKPosition(AvatarIKGoal.LeftHand, lHandPoint.position);
        }
        else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
        }
    }
}
