﻿using Dreamteck.Splines;
using RagdollMecanimMixer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoldier : Soldier
{
    [HideInInspector] public PlayerSquad playerSquad;

    [SerializeField] private float maxHealth;

    public override void Setup(Squad squad)
    {
        base.Setup(squad);

        helmet = transform.GetComponentInChildren<Helmet>();

        weaponContainer = transform.GetComponentInChildren<WeaponContainer>();

        weaponContainer.Setup(this);

        weapon = weaponContainer.weapon;

        soldierData = squad.soldierData;

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        animator = transform.GetComponentInChildren<Animator>();

        ramecanMixer = transform.GetComponentInChildren<RamecanMixer>();

        var mr = transform.GetComponentInChildren<Renderer>();

        mr.material = soldierData.soldierMaterial;

        splineProjector = gameObject.AddComponent<SplineProjector>();

        splineProjector.projectTarget = transform;

        splineProjector.spline = squad.road.splineComputer;

        splineProjector.RebuildImmediate();

        splineProjector.CalculateProjection();

        var sample = squad.road.splineComputer.Evaluate(splineProjector.result.percent);

        var dir = transform.position - sample.position;

        sideOffset = dir.magnitude * Mathf.Sign(Vector3.Dot(dir.normalized, sample.right));

        roadLength = squad.road.splineComputer.CalculateLength();

        roadDistance = roadLength * (float)sample.percent;

        health = maxHealth;
    }
}
