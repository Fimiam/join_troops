﻿using Dreamteck.Splines;
using RagdollMecanimMixer;
using System;
using UnityEngine;
using UnityEngine.AI;

public class PlayerSoldier : Soldier
{
    private void SetupNeutralSoldiersDetector()
    {
        var go = Resources.Load("prefabs/neutralDetector") as GameObject;

        var detector = Instantiate(go).GetComponent<NeutralDetector>();

        detector.Setup(this);

        detector.transform.parent = transform;

        detector.transform.localPosition = Vector3.zero;
    }

    public override void Setup(Squad squad)
    {
        base.Setup(squad);

        SetupNeutralSoldiersDetector();

        health = 200;
    }

    public void TouchNeutral(NeutralSoldier neutral)
    {
        squad.AddNeutral(neutral);
    }
}
