using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helmet : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;

    [SerializeField] private List<Collider> colliders;

    public void SoldierKilled()
    {
        transform.parent = null;

        rb.isKinematic = false;

        colliders.ForEach(c => c.enabled = true);

        var pushForce = Random.onUnitSphere * 3;

        pushForce.y = Mathf.Abs(pushForce.y);

        rb.AddForce(pushForce, ForceMode.Impulse);
    }
}
