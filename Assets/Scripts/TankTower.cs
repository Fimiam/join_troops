using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankTower : MonoBehaviour
{
    [SerializeField] private TankBullet bullet_prefab;

    [SerializeField] private Transform muzzleBulletPoint;


    private bool canFire = true;

    public bool CanFire => canFire;


    private float toNextFire;

    [SerializeField] private float fireRate;

    [SerializeField] private float switchSideSpeed;

    public void Fire(SquadUnit target)
    {
        enabled = true;

        if(!canFire)
        {
            return;
        }

        canFire = !canFire;

        toNextFire = fireRate;

        var bullet = Instantiate(bullet_prefab);

        bullet.transform.position = muzzleBulletPoint.position;

        var dir = (target.transform.position + Vector3.up) - transform.position;

        bullet.transform.rotation = Quaternion.LookRotation(dir.normalized);

        bullet.Shoot(target);
    }

    private void Update()
    {
        toNextFire -= Time.deltaTime;

        if(toNextFire < 0f)
        {
            canFire = true;
            enabled = false;
        }
    }
}
