﻿using UnityEngine;

[System.Serializable]
public class SoldierData
{
    public float sideSpeed;

    public float forwardSpeed, forwardAccelSmooth, forwardStopSmooth;

    public float turAlign = .98f, turnForce = 3, turnSmooth = 4;

    public Material soldierMaterial;
}
