﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySquad : Squad
{

    protected override void Update()
    {
        if (enemyUnits.Count > 0)
        {
            WarUpdate();

            return;
        }
    }

    private void WarUpdate()
    {
        foreach (var item in members)
        {

            if (item.Target == null)
            {
                SetTarget(item);
            }
            else
            {
                if (item.Target.dead)
                {
                    enemyUnits.Remove(item.Target);
                    item.SetTarget(null);
                }
            }
        }
    }

    private void SetTarget(SquadUnit unit)
    {
        float min = float.MaxValue;

        SquadUnit closest = enemyUnits[0];

        foreach (var item in enemyUnits)
        {
            if (item == null) continue;

            var distance = (item.transform.position - unit.transform.position).sqrMagnitude;

            if (min > distance)
            {
                min = distance;

                closest = item;
            }
        }

        unit.SetTarget(closest);
    }

    private void OnTriggerEnter(Collider other)
    {
        var soldier = other.transform.GetComponentInParent<PlayerSoldier>();

        if(soldier)
        {
            GetComponent<Collider>().enabled = false;

            var playerSquad = soldier.Squad;

            playerSquad.AddEnemySquad(this);

            //foreach (var item in members)
            //{
                //playerSquad.AddEnemySoldier(item as EnemySoldier);
            //}

            //foreach (var item in playerSquad.members)
            //{
                
            //}
        }
    }
}
