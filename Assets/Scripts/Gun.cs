﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon
{
    [SerializeField] private ParticleSystem muzzleFlashEffect;

    [SerializeField] private Transform muzzlePoint;

    [SerializeField] private GunBullet bullet_prefab;

    public override void Fire(SquadUnit target)
    {
        base.Fire(target);

        var bullet = Instantiate(bullet_prefab);

        bullet.transform.position = muzzlePoint.position;

        bullet.Shoot(target);

        muzzleFlashEffect.Play();
    }
}
