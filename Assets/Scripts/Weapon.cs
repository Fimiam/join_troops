﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Weapon : MonoBehaviour
{
    public float fireRange;
    public bool fireReady;
    public float fireRate;
    public float damage;

    public Transform rHandPoint, lHandPoint;

    private float toNextFire;

    private Soldier soldier;

    public void Setup(Soldier soldier)
    {
        this.soldier = soldier;
    }

    private void Update()
    {
        toNextFire -= Time.deltaTime;

        fireReady = toNextFire < 0.0f;
    }

    public virtual void Fire(SquadUnit target)
    {
        toNextFire = fireRate;
    }

    public void SoldierKilled()
    {
        if (!enabled) return;

        enabled = false;

        transform.parent = null;

        var rb = gameObject.AddComponent<Rigidbody>();

        rb.AddForce(Random.onUnitSphere * Random.Range(3, 8), ForceMode.Impulse);
    }
}
