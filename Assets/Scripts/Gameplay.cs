using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    [SerializeField] private UIController uiController;

    public void PlayerSquadKilled()
    {
        uiController.ShowGameover();

        var enemySoldiers = FindObjectsOfType<EnemySoldier>();

        foreach (var item in enemySoldiers)
        {
            item.VictoryDance();
        }
    }
}
