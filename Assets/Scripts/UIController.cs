﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    [SerializeField] private Image fadingImage;
    [SerializeField] private GameObject gameoverPanel;

    private void Start()
    {
        fadingImage.DOFade(0, .6f);
    }

    public void ShowGameover()
    {
        gameoverPanel.SetActive(true);

        //StartCoroutine(Restarting());
    }

    private IEnumerator Restarting()
    {
        yield return new WaitForSeconds(2.4f);

        fadingImage.DOFade(1, .6f).onComplete += () => SceneManager.LoadScene(0);
    }
}
