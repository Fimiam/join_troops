using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWheel : MonoBehaviour
{
    [SerializeField] private float moveSens = 120;

    public void UpdateMoveDelta(float move)
    {
        transform.Rotate(Vector3.right * move * moveSens * Time.deltaTime);
    }
}
