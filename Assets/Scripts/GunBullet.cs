using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBullet : MonoBehaviour
{
    private Vector3 movementDirection;

    [SerializeField] private ParticleSystem hitEffect_prefab;

    [SerializeField] private float speed;

    [SerializeField] private float lifetime = 5;

    [SerializeField] private float damage;

    private float remain;

    private SquadUnit target;
    
    void Update()
    {
        remain -= Time.deltaTime;

        if(remain < 0)
        {
            Destroy(gameObject);
        }

        transform.position += movementDirection * speed * Time.deltaTime;
    }

    public void Shoot(SquadUnit target)
    {
        this.target = target;

        remain = lifetime;

        var dir = target.transform.position - transform.position;

        dir.y = 0;

        movementDirection = dir.normalized;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var soldier = collision.transform.GetComponentInParent<Soldier>();

        if(soldier == this.target)
        {
            var rb = collision.transform.GetComponent<Rigidbody>();

            if (rb)
            {
                rb.AddForce(movementDirection * 3, ForceMode.Impulse);
            }
            
            soldier.GetHit(damage, movementDirection);


            var effect = Instantiate(hitEffect_prefab);

            effect.transform.position = transform.position;

            Destroy(gameObject);
            Destroy(effect.gameObject, 3);
        }
    }
}
