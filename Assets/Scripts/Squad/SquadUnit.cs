using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadUnit : MonoBehaviour
{
    protected Squad squad;

    public Squad Squad => squad;

    protected float health;

    public float sideOffset;

    protected float roadDistance;

    protected SquadUnit target;

    public SquadUnit Target => target;

    public bool dead;

    public virtual void Setup(Squad squad)
    {
        this.squad = squad;
    }

    public virtual void AddSideOffset(float sideMove)
    {
        
    }

    public virtual void ForwardMove(float forwardStep)
    {
        
    }

    public virtual void UpdateMove()
    {
        
    }

    public virtual void UpdateMoveAnim(float v)
    {
        
    }

    public virtual void SetTarget(SquadUnit target)
    {
        this.target = target;
    }

    protected virtual void Update()
    {

    }
}
