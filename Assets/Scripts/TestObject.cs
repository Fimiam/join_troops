﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestObject : MonoBehaviour
{
    [SerializeField] private Transform obj;

    // Update is called once per frame
    void Update()
    {
        Vector3 offset = transform.rotation * (obj.position - transform.position);

        Debug.Log($"x - {offset.x}");
        Debug.Log($"y - {offset.y}");
    }
}
