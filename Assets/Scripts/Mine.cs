﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    [SerializeField] private ParticleSystem explosionEffect_prefab, timerEffect_prefab;

    [SerializeField] private float explosionTimer;

    [SerializeField] private SphereCollider trigger;

    private Coroutine explosionRoutine;

    private void OnTriggerEnter(Collider other)
    {
        var soldier = other.transform.GetComponentInParent<PlayerSoldier>();

        if(soldier)
        {
            trigger.enabled = false;

            StartCoroutine(Explosion());
        }
    }

    //private IEnumerator Detection()
    //{
    //    var radius = GetComponent<SphereCollider>().radius;

    //    yield return null;

    //    Collider[] overlaps = Physics.OverlapSphere(transform.position, radius);

    //    foreach (var item in overlaps)
    //    {
    //        var soldier = item.GetComponent<PlayerSoldier>();

    //        if(soldier)
    //        {
    //            trigger.enabled = false;

    //            StartCoroutine(Explosion());
    //        }
    //    }
    //}

    private IEnumerator Explosion()
    {
        var timerEffect = Instantiate(timerEffect_prefab);

        timerEffect.transform.position = transform.position;

        yield return new WaitForSeconds(explosionTimer);

        var explosionEffect = Instantiate(explosionEffect_prefab);

        explosionEffect.transform.position = transform.position;

        List<PlayerSoldier> affectedSoldier = new List<PlayerSoldier>();

        Collider[] overlap = Physics.OverlapSphere(transform.position, trigger.radius * 1.5f);

        foreach (var item in overlap)
        {
            var soldier = item.transform.GetComponentInParent<PlayerSoldier>();

            if(soldier && !affectedSoldier.Contains(soldier))
            {
                affectedSoldier.Add(soldier);
            }
        }

        foreach (var item in affectedSoldier)
        {
            item.Explosion(transform.position, 4000, trigger.radius * 1.2f);
        }

        gameObject.SetActive(false);

        timerEffect.gameObject.SetActive(false);

        Destroy(explosionEffect.gameObject, 2);
    }
}
