﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeutralDetector : MonoBehaviour
{
    private Soldier soldier;

    private void OnTriggerEnter(Collider other)
    {
        var neutral = other.transform.GetComponentInParent<NeutralSoldier>();

        if(neutral)
        {
            soldier.Squad.AddNeutral(neutral);
        }
    }

    public void Setup(PlayerSoldier playerSoldier)
    {
        soldier = playerSoldier;
    }
}
