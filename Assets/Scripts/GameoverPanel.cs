﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameoverPanel : MonoBehaviour
{
    [SerializeField] private RectTransform backgroundRect, textRect;

    private void Start()
    {
        backgroundRect.DOAnchorPos(Vector2.zero, .65f);

        textRect.DOAnchorPos(Vector2.zero, .8f);
    }
}
