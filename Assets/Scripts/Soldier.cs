﻿using Dreamteck.Splines;
using RagdollMecanimMixer;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

public class Soldier : SquadUnit
{
    private readonly int[] danceAnims = new int[] { Animator.StringToHash("dance_1"), Animator.StringToHash("dance_2"),
        Animator.StringToHash("dance_3"), Animator.StringToHash("dance_4"), Animator.StringToHash("dance_5") };

    protected Weapon weapon;

    public float moveSmooth = 5;

    protected SoldierData soldierData;

    private static int forwardAnim = Animator.StringToHash("forwardSpeed");
    private static int firingAnimBool = Animator.StringToHash("firing");

    protected NavMeshAgent agent;

    protected Animator animator;

    protected RamecanMixer ramecanMixer;

    protected bool warState;

    protected WeaponContainer weaponContainer;

    protected float roadLength;

    protected SplineProjector splineProjector;

    protected Helmet helmet;


    public override void Setup(Squad squad)
    {
        base.Setup(squad);

        helmet = transform.GetComponentInChildren<Helmet>();

        weaponContainer = transform.GetComponentInChildren<WeaponContainer>();

        weaponContainer.Setup(this);

        weapon = weaponContainer.weapon;

        soldierData = squad.soldierData;

        agent = GetComponent<NavMeshAgent>();

        animator = transform.GetComponentInChildren<Animator>();

        ramecanMixer = transform.GetComponentInChildren<RamecanMixer>();

        var mr = transform.GetComponentInChildren<Renderer>();

        mr.material = soldierData.soldierMaterial;

        splineProjector = gameObject.AddComponent<SplineProjector>();

        splineProjector.projectTarget = transform;

        splineProjector.spline = squad.road.splineComputer;

        splineProjector.RebuildImmediate();

        splineProjector.CalculateProjection();

        var sample = squad.road.splineComputer.Evaluate(splineProjector.result.percent);

        var dir = transform.position - sample.position;

        sideOffset = dir.magnitude * Mathf.Sign(Vector3.Dot(dir.normalized, sample.right));

        roadLength = squad.road.splineComputer.CalculateLength();

        roadDistance = roadLength * (float)sample.percent;
    }

    protected override void Update()
    {
        warState = target != null;

        if(warState)
        {
            agent.enabled = true;

            var distance = (transform.position - target.transform.position).magnitude;

            var firingRange = distance < weapon.fireRange;

            agent.enabled = !firingRange;

            if (!firingRange)
            {
                agent.SetDestination(target.transform.position);

                animator.SetFloat(forwardAnim, 1.0f);
            }
            else
            {
                if (weapon != null && weapon.fireReady)
                    weapon.Fire(target);
            }

            animator.SetBool(firingAnimBool, !agent.enabled);
        }
        else
        {
            animator.SetBool(firingAnimBool, false);
        }

        enabled = warState;

        if (!warState || weapon == null)
        {
            agent.enabled = false;

            return;
        }
    }

    public override void SetTarget(SquadUnit target)
    {
        base.SetTarget(target);

        enabled = true;

        if (target == null) RecalculateRoadPosition();
    }

    private void RecalculateRoadPosition()
    {
        splineProjector.CalculateProjection();

        var sample = squad.road.splineComputer.Evaluate(splineProjector.result.percent);

        roadDistance = roadLength * (float)splineProjector.result.percent;

        var dir = transform.position - sample.position;

        sideOffset = dir.magnitude * Mathf.Sign(Vector3.Dot(dir.normalized, sample.right));
    }

    public override void UpdateMoveAnim(float forwardSpeed)
    {
        animator.SetFloat(forwardAnim, forwardSpeed);
    }

    public override void AddSideOffset(float value)
    {
        var valSign = Mathf.Sign(value);

        var offsetSign = Mathf.Sign(sideOffset);

        if(value != 0.0f)
        {
            if (valSign != offsetSign)
            {
                splineProjector.CalculateProjection();

                var sample = squad.road.splineComputer.Evaluate(splineProjector.result.percent);

                var dir = transform.position - sample.position;

                sideOffset = dir.magnitude * Mathf.Sign(Vector3.Dot(dir.normalized, sample.right));
            }
        }

        sideOffset += value;
    }

    public override void ForwardMove(float step)
    {
        roadDistance += step;
    }

    public void VictoryDance()
    {
        int anim = Random.Range(0, danceAnims.Length);

        if (weapon) weapon.gameObject.SetActive(false);

        animator.SetTrigger(danceAnims[anim]);
    }

    public void GetKnoked(Vector3 direction, int force)
    {
        if (dead) return;

        ramecanMixer.BeginStateTransition(1);

        foreach (var rb in transform.GetComponentsInChildren<Rigidbody>())
        {
            rb.AddForce(force * (direction + Vector3.up), ForceMode.Impulse);
        }

        health = 0;

        Killed();
    }

    public void GetHit(float damage, Vector3 direction)
    {
        health -= damage;

        if(health < 0)
        {
            GetKnoked(direction, 13);
        }
    }

    public void Explosion(Vector3 position, float force, float radius)
    {
        ramecanMixer.BeginStateTransition(1);

        foreach (var rb in transform.GetComponentsInChildren<Rigidbody>())
        {
            rb.AddExplosionForce(force, position, radius);
        }

        Killed();
    }

    private void Killed()
    {
        if (dead) return;

        enabled = false;

        Destroy(this);

        dead = true;

        animator.enabled = false;
        squad.MemberKilled(this);

        if (weapon) weapon.SoldierKilled();

        if (helmet) helmet.SoldierKilled();
    }

    public override void UpdateMove()
    {
        var sample = squad.road.splineComputer.Evaluate(roadDistance / roadLength);

        var nextPosition = sample.position + sample.right * sideOffset;

        var moveDelta = nextPosition - transform.position;
        
        transform.position = nextPosition;

        Quaternion targetRotation = default;

        if(moveDelta.magnitude > 0.0f)
        {
            targetRotation = Quaternion.LookRotation(moveDelta.normalized);
        }
        else
        {
            targetRotation = Quaternion.LookRotation(squad.squadRoadSample.forward);
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 8 * Time.deltaTime);
    }
}
