﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField] private PlayerSquad squad;

    [SerializeField] private Vector3 offset, lookOffset;

    [SerializeField] private float smoothPosition = 4f, smoothLook = 4f;
  
    void LateUpdate()
    {
        var nextPos = squad.roadPoint.position + squad.roadPoint.rotation * offset;

        transform.position = Vector3.Lerp(transform.position, nextPos, smoothPosition * Time.deltaTime);

        var nextRot = Quaternion.LookRotation(squad.roadPoint.position + lookOffset - transform.position);

        transform.rotation = Quaternion.Lerp(transform.rotation, nextRot, smoothLook * Time.deltaTime);
    }
}
