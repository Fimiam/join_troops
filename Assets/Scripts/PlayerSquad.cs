﻿using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSquad : Squad
{
    [SerializeField] private SplineProjector projector;

    [SerializeField] private float sideSense = 20;

    private Bounds squadBounds;

    private Vector3 centerPoint, targetCenterPoint;

    private Transform projectObject;
    public Transform roadPoint;

    private float currentForwardSpeed;

    private float sideDistance, currentSide;

    protected override void Start()
    {
        base.Start();

        projectObject = new GameObject("projectObject").transform;
        roadPoint = new GameObject("roadPoint").transform;

        projector.projectTarget = projectObject;

        projector.spline = road.splineComputer;

        projector.RebuildImmediate();

        squadBounds = new Bounds(members[0].transform.position, Vector3.zero);

        targetCenterPoint = centerPoint = squadBounds.center;


        //members.ForEach(m => AddGunToSoldier(m));

    }

    protected override void Update()
    {
        if(enemyUnits.Count > 0)
        {
            WarUpdate();

            return;
        }

        SquadInput();

        SquadRoadPoint();

        Movement();
    }

    private void WarUpdate()
    {
        foreach (var item in members)
        {

            if(item.Target == null)
            {
                SetTarget(item);
            }
            else
            {
                if(item.Target.dead)
                {
                    enemyUnits.Remove(item.Target);
                    item.SetTarget(null);
                }
            }
        }

        SquadRoadPoint();
    }

    private void SetTarget(SquadUnit soldier)
    {
        float min = float.MaxValue;

        SquadUnit closest = enemyUnits[0];

        foreach (var item in enemyUnits)
        {
            if (item == null) continue;

            var distance = (item.transform.position - soldier.transform.position).sqrMagnitude;

            if(min > distance)
            {
                min = distance;

                closest = item;
            }
        }

        soldier.SetTarget(closest);
    }

    public void AddEnemySoldier(EnemySoldier enemySoldier)
    {
        if (enemyUnits.Contains(enemySoldier)) return;

        enemySoldier.playerSquad = this;

        enemyUnits.Add(enemySoldier);
    }

    private void RemoveEnemySoldier(EnemySoldier enemySoldier)
    {
        enemyUnits.Remove(enemySoldier);
    }

    private void SquadInput()
    {
        float currSide = 0;

        if(sideDistance != 0)
            currSide = Mathf.Sign(sideDistance);

        float input = InputController.PlayerInput.x * sideSense;

        float inputSide = 0;

        if (input != 0)
            inputSide = Mathf.Sign(input);

        if ((currSide != 0 && inputSide != 0) && inputSide != currSide)
        {
            sideDistance = 0;
        }

        sideDistance += input;

        currentSide = Mathf.Sign(sideDistance);
    }    

    private void Movement()
    {
        float forwardStep = default;

        float sideMove = default;

        if (!InputController.Hold)
        {
            sideDistance = 0;

            currentForwardSpeed = Mathf.MoveTowards(currentForwardSpeed, 0, soldierData.forwardStopSmooth * Time.deltaTime);
        }
        else
        {
            currentForwardSpeed = Mathf.MoveTowards(currentForwardSpeed, soldierData.forwardSpeed, soldierData.forwardAccelSmooth * Time.deltaTime);

            float sideSpeed = soldierData.sideSpeed * (currentForwardSpeed / soldierData.forwardSpeed);

            float prevDistance = sideDistance;

            sideDistance = Mathf.MoveTowards(sideDistance, 0, sideSpeed * Time.deltaTime);

            sideMove = prevDistance - sideDistance;

            if (sideMove != 0)
            {
                var rightInput = sideMove > 0;

                bool maxOffset = false;

                foreach (var item in members)
                {
                    var offset = item.sideOffset;

                    if (rightInput)
                    {
                        if (offset + sideMove > road.width * .5f)
                        {
                            maxOffset = true;
                            break;
                        }
                    }
                    else
                    {
                        if (offset + sideMove < road.width * -.5f)
                        {
                            maxOffset = true;
                            break;
                        }
                    }
                }

                if (maxOffset)
                {
                    sideMove = 0;
                }
            }

        }

        forwardStep = currentForwardSpeed * Time.deltaTime;

        foreach (var item in members)
        {
            item.AddSideOffset(sideMove);
            item.ForwardMove(forwardStep);
            item.UpdateMove();
            item.UpdateMoveAnim(currentForwardSpeed / soldierData.forwardSpeed);
        }
    }

    protected override void RemoveMember(Soldier soldier)
    {
        members.Remove(soldier as PlayerSoldier);

        if (members.Count < 1)
        {
            var gameplay = FindObjectOfType<Gameplay>();

            gameplay.PlayerSquadKilled();
        }
    }

    private void SquadRoadPoint()
    {
        var boundsPoint = centerPoint != Vector3.zero ? centerPoint : members[0].transform.position;

        squadBounds = new Bounds(boundsPoint, Vector3.zero);

        members.ForEach(m => squadBounds.Encapsulate(m.transform.position));

        targetCenterPoint = squadBounds.center;

        centerPoint = Vector3.MoveTowards(centerPoint, targetCenterPoint, 12 * Time.deltaTime);

        projectObject.position = centerPoint;

        projector.CalculateProjection();

        squadRoadSample = road.splineComputer.Evaluate(projector.result.percent);

        roadPoint.position = squadRoadSample.position;
        roadPoint.rotation = squadRoadSample.rotation;
    }

    protected override void AddMember(Soldier soldier)
    {
        base.AddMember(soldier);

        //AddGunToSoldier(soldier);
    }

    //private void AddGunToSoldier(Soldier soldier)
    //{
    //    var gun_prefab = Resources.Load("prefabs/guns/gun");

    //    var gun = Instantiate(gun_prefab) as GameObject;

    //    soldier.SetWeapon(gun.GetComponent<Weapon>());
    //}
}
