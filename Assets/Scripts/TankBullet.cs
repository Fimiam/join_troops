using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBullet : MonoBehaviour
{
    private SquadUnit target;

    [SerializeField] private float lifetime = 3, speed = 5, explosionRadius = 1;

    [SerializeField] private GameObject explosionParticles_prefab;

    private float remain;

    public void Shoot(SquadUnit target)
    {
        this.target = target;

        remain = lifetime;
    }

    private void Update()
    {
        remain -= Time.deltaTime;

        if (remain < 0)
        {
            Destroy(gameObject);
        }

        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {

        Debug.Log(collision.collider.name);

        var point = collision.contacts[0].point;

        transform.GetComponentInChildren<Renderer>().enabled = false;

        Destroy(gameObject, 5);

        StartCoroutine(ExplosionRoutine(point));
    }

    private IEnumerator ExplosionRoutine(Vector3 point)
    {
        Collider[] colls = Physics.OverlapSphere(point, explosionRadius);

        var explosionFX = Instantiate(explosionParticles_prefab);

        explosionFX.transform.position = transform.position;

        var targets = new List<Soldier>();

        for (int i = 0; i < colls.Length; i++)
        {
            var rb = colls[i].GetComponent<Rigidbody>();

            var dir = colls[i].transform.position - point;

            if(rb)
            {
                rb.AddForce(dir.normalized * 3, ForceMode.Impulse);
            }

            var soldier = colls[i].GetComponentInParent<Soldier>();

            if(soldier && !targets.Contains(soldier))
            {
                targets.Add(soldier);
                soldier.GetHit(51, dir.normalized);
            }

            yield return null;
        }
    }
}
