﻿using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    public SplineComputer splineComputer;

    [HideInInspector] public float length;

    public float width;

    void Awake()
    {
        length = splineComputer.CalculateLength();
    }
}
