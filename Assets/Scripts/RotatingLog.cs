﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingLog : MonoBehaviour
{
    [SerializeField] private ParticleSystem hitEffect_prefab;

    private List<PlayerSoldier> affected = new List<PlayerSoldier>();

    private void OnCollisionEnter(Collision other)
    {
        var soldier = other.transform.GetComponentInParent<PlayerSoldier>();

        if(soldier && !affected.Contains(soldier))
        {
            var hitPoint = other.contacts[0].point;

            var effect = Instantiate(hitEffect_prefab);

            effect.transform.position = hitPoint;

            var direction = soldier.transform.position - hitPoint;

            var force = 40;

            soldier.GetKnoked(direction, force);

            affected.Add(soldier);
        }
    }
}
